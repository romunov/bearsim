# Bear Simulation

We are trying to mimic different management plans in Croatia and Slovenia.
Croatia implements the take of adult (males) whereas Slovenia mandates the take
of younger individuals (~2.5 y/o).

# Simulation mechanics

We generate two populations of size `n`. In each, a proportion of males and females are generated. Each individual is given a genotype.

Population is evolved through 'generations'. Each generation goes through `preOps`, `mating(Scheme)` and `postOps`.

Explanation of `infoFields`:

  * `age`: age of individual (in years)
  * `ywc`: years with cubs. This is a bookkeeping variable (initial value of 0 for all generated individuals)
  * `hc`: a binary operator designating female having cubs or not
  * `iscub`: is animal a cub or (sub)adult. A bookkeeping variable (initial value of 0 for all generated individuals)
  * `father_idx`, `mother_idx`: a bookkeeping variable which identifies individual's parents
  * `ind_id`: tracks individuals
  * `migrate_to`: bookkeeping variable to keep track of migrated individuals
  * `me`: number of times a male mates
 
### Initial population
  * for each country, a population size is specified and initial population is the size of the sum of these two populations
  * used loci are 5 and `infoFields` are described above
  * ages are random values from a binomial distribution with parameters `n = 3` and `p = 0.4`
  * sex ratio is 0.5
  * initial alleles are random numbers from 0 to 9

  
### preOps:
  * each individual ages by 1
  * if mother is rearing cubs, a variable recording duration of rearing is increased by 1
  * if cub has been with mother for `primiparity` years, cub becomes independent, `iscub` is switched to 0
  * mother with cubs a sufficient time (`primiparity`) releases cubs and is in estrus again, `ywc` and `hc` are switched to 0
  * starting at the second evolution (step 1), young males (2 <= age < 4) migrate from Croatian to Slovenian subpopulation according to a specified proportion
  
### matingScheme

  * all females of age `>= primiparity`  with no cubs mate with probability set by parameter `mate_rate`
  * males mate according to age; older males call dibs and progressively younger males get access (as implemented for females in Wiegand et al. 1998). Males mate `me` times each
  * cubs are born with `age = 0` and `iscub = 1`. Genotypes are transferred from parents to offspring, along with parents tags
  * parents are copied to the next generation after offspring is produced
  
### postOps
  * cull is performed according to management in subpopulation based on categorical distribution where weights for specific groups are hard coded into the functions.
  Details on how the weights are derived are available in `/fit_age_structure/fit_age_at_cull.Rmd`
  * effective population size using linkage disequilibrium (Ne_LD_sp) is calculated for each subpopulation according to Waples and Do (2010).
  Calculations are done on a defined sample size (e.g. `sim.PyOperator(func = splitCalcMerge, param = {"croatia": 100, "slovenia": 100})`). 
  If the population is smaller than the defined parameter, population is split in half and calculations are done on one half which is later
  merged with the other split half. See section on [effective size](http://simupop.sourceforge.net/manual_svn/build/refManual_ch3_sec11.html?highlight=stat#Stat) of the simuPOP software