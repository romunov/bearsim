# -*- coding: utf-8 -*-

import collections as collections
import numpy as np
import random as random
import simuPOP as sim

'''
# This function performs cull for male and female animals according to the take
# set by parameters "p.male" and "p.female" in argument `param`.

@author: Roman Luštrik
'''

def cullCountry(pop, param):
    # N = number of culled (m)ales and (f)emales

    # Sample N "individuals" from a discrete distribution
    # range of culled male bears is from 0 to 13 years of age
    n_males = np.random.choice(a = range(len(param["p.male"])), size = param["Nm"], replace = True,
                     p = param["p.male"])
    n_males = collections.Counter(n_males)

    # range of culled female bears is from 0 to 16
    n_females = np.random.choice(a = range(len(param["p.female"])), size = param["Nf"], replace = True,
                     p = param["p.female"])
    n_females = collections.Counter(n_females)

    ## Prepare female and male individuals
    all_males = [x for x in pop.individuals() if x.sex() == sim.MALE]

    # from the prepared list sample out v number of individuals from corresponding
    # k age groups
    out_males = list()
    for k, v in n_males.items(): # iterating through tuples is key:value
        males_of_age = [x for x in all_males if x.age == k] # these are the males from age group v

        # This try statement is necessary because if a certain age group has insufficient number of animals to sample
        # from, try catch truncates the sampling to only the number of available animals.
        try:
            out_males.append(random.sample([x for x in males_of_age], v)) # take out appropriate number of individuals, no replacement
            # print("cullByCountry: MALEs: group: {} | available: {} | required: {}".format(k, len(males_of_age), v))
        except ValueError:
            # print("cullByCountry: MALEs: group: {} | available: {} | required: {} (truncating)".format(k, len(males_of_age), v))
            num_avail = len(males_of_age)
            out_males.append(random.sample([x for x in males_of_age], num_avail))

    # from the prepared list sample out v number of individuals from corresponding
    # k age groups
    all_females = [x for x in pop.individuals() if x.sex() == sim.FEMALE and x.hc == 0]

    out_females = list()
    # print("n_females{}".format(len(n_females.items())))
    for k, v in n_females.items(): # k = key, v = value in a tuple
        females_of_age = [x for x in all_females if x.age == k]


        # This try statement is necessary because if a certain age group has insufficient number of animals to sample
        # from, try catch truncates the sampling to only the number of available animals.

        try:
            out_females.append(random.sample(females_of_age, v))
            # print("cullByCountry: FEMALEs: group: {} | available: {} | required: {}".format(k, len(females_of_age), v))

        except ValueError:
            # print("cullByCountry: FEMALEs: group: {} | available: {} | required: {} (truncating)".format(k, len(females_of_age), v))

            num_avail = len(females_of_age)
            out_females.append(random.sample([x for x in females_of_age], num_avail))

    # Make lists flat.
    out_females = [item for sublist in out_females for item in sublist]
    out_males = [item for sublist in out_males for item in sublist]

    # Extract
    out_females_ids = [x.ind_id for x in out_females]
    out_males_ids = [x.ind_id for x in out_males]

    # before_cull = pop.subPopSize()
    # print("cullByCountry: pop before cull: {}".format(before_cull))
    # Send lucky ones to oblivion
    pop.removeIndividuals(IDs = out_males_ids, idField = "ind_id")
    pop.removeIndividuals(IDs = out_females_ids, idField = "ind_id")
    # print("CULL: pop after cull: {}".format(pop.subPopSize()))
    # after_cull = pop.subPopSize()

    # print("cullByCountry: Removed number of individuals: {}".format(before_cull - after_cull))

    return True