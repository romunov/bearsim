# -*- coding: utf-8 -*-

'''
Functions used to debug or follow certain variables during simulation.

@author Roman Luštrik (roman.lustrik@biolitika.si)
'''

from constants import fam_size
from constants import fam_size_it

'''
Calculate predicted population sizes based on number of mating females. Each female is assigned a random number of
cubs.
'''
def endGenerationClick():
    print("=== END OF GENERATION TALLY ===")
    print("fam_size: {}". format(fam_size))
    print("fam_size_it: {}". format(fam_size_it))
    print("=== END OF GENERATION TALLY ===")
    return True