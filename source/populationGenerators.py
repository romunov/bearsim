# -*- coding: utf-8 -*-

'''
Calculate population size based on number of females. Females mate with a certain probability (set by `mate_rate`
constant).
'''

import simuPOP as sim
import random as random
import numpy as np
from constants import *
import sys

def calculatePopulationSize(pop):
    global fam_size
    fam_size = [[], []]

    for sub in range(pop.numSubPop()):
        # for each female sample litter size and store it in 'x'
        x = []
        for ind in pop.individuals(subPop = sub):
            if ind.sex() == sim.FEMALE and ind.hc == 0 and ind.age >= primiparity and random.random() >= mate_rate:
                x.append(np.random.choice(a = [1, 2, 3, 4], size = 1, p = [0.15, 0.65, 0.15, 0.05], replace = True).tolist())

        # Calculate the number of suitable males that can produce offspring.
        suitable_males = [bear for bear in pop.individuals(subPop = sub) if bear.sex() == sim.MALE and bear.iscub == 0]
        males_can_produce_litters = len(suitable_males) * me

        # And see if the number of available males is sufficient to produce all litters in `x`
        # print("[calculatePopulationSize] {}/{} litter size/male capacity".format(x, males_can_produce_litters))
        # This piece of code will remove one litter at a time, until males are able to hold their end of the bargain.
        x = sum(x, [])

        while males_can_produce_litters < len(x):
            if males_can_produce_litters == 0:
                print("[calculatePopulationSize] males: {} | theoretical litters: {} | litters needed: {}".format(len(suitable_males), males_can_produce_litters, len(x)))
                sys.exit("No males to father offspring in at least one subpopulation.")
            # print("[calculatePopulationSize] correcting for insufficient number of males")
            x = x[1:] # throw out first litter
            print("[calculatePopulationSize] males: {} | theoretical litters: {} | litters needed: {}".format(len(suitable_males), males_can_produce_litters, len(x)))

        # I want to incorporate a scenario where not all females get to mate/produce offspring (same thing)
        # np.random.choice(a = range(len(x)), size = round(p_mate * len(x)), replace = False)

        fam_size[sub] = x
        # print("number of added cubs in pop {}: {} (total {})".format(sub, fam_size[sub]), sum(fam_size[sub]))

    # sum population size and extra cubs population-wise
    post_rep_popsize = []

    for i in range(pop.numSubPop()):
        # print("[calculatePopulationSize] subPopSizes: {} added {} cubs (total {})".format(pop.subPopSizes()[i], sum(fam_size[i]), pop.subPopSizes()[i] + sum(fam_size[i])))
        post_rep_popsize.append(pop.subPopSizes()[i] + sum(fam_size[i]))

    # make 'fam_size' a flat list
    fam_size = sum(fam_size, [])

    # print("calculatePopulationSize: {}".format(fam_size))


    global fam_size_it
    fam_size_it = fam_size
    # return final population size which consists of cloned non-offspring
    # and new offspring
    # print("calculatePopulationSize: population size {}".format(post_rep_popsize))
    # print("===")
    return post_rep_popsize

# Read family size from a global variable and use that as
# the number of offspring per mating event. This function is applied
# to individual subpopulation
def familySizeGenerator():
    global fam_size_it

    # print("familySizeGenerator: === population ===")
    # print("familySizeGenerator: fam_size_it = {}".format(fam_size_it))
    # print("familySizeGenerator:    fam_size = {}".format(fam_size))
    for sz in fam_size_it:
        # print("familySizeGenerator: producing {} cubs".format(sz,))
        fam_size_it = fam_size_it[1:] # after cub has been "made", remove it
        yield sz