# -*- coding: utf-8 -*-

import simuPOP as sim
import numpy as np
from constants import me
from itertools import groupby
import collections
'''
Function will pick a father according to the rules "jači tlači". Bears are sorted according to age (size) and
largest males get to mate "me" (mating events) times.

@author Roman Luštrik
'''

def bearFather(pop, subPop):

    ##### debugging:
    # find if cubs are being turned into adults
    # iscub = [y.iscub for y in pop.individuals(subPop)]
    # print("bearFather ratio of iscubs: {}".format([len(list(group)) for key, group in groupby(iscub)]))
    #####

    # find all illegible males that are not cubs
    all_males = [x for x in pop.individuals(subPop) if x.sex() == sim.MALE and x.iscub == 0]

    # sort them by age
    # print("bearFather: number of suitable males: {}".format(len(all_males)))
    # print([x.age for x in all_males])
    all_males = sorted(all_males, key = lambda k: k.age, reverse = True)

    ### debugging
    # display proportion of males
    sim.stat(pop = pop, numOfMales = True, vars = ["propOfMales_sp", "numOfMales_sp"])
    sim.stat(pop = pop, popSize = True, vars = "subPopSize")

    # from simuPOP.utils import viewVars
    # viewVars(pop.dvars())
    # print("bearFather: {}/{} (prop/size)".format(pop.dvars(0).propOfMales, pop.dvars(0).numOfMales,
    #                                              pop.dvars(1).propOfMales, pop.dvars(1).numOfMales))

    # if pop.dvars().gen % 5 == 0: # print for every fifth generation
    #     print("[bearFather](gen={}) {} of {} (cro), {} of {} (slo)".format(pop.dvars().gen, round(pop.dvars(0).propOfMales, 2), pop.dvars().subPopSize[0], round(pop.dvars(1).propOfMales, 2), pop.dvars().subPopSize[1]))

    # all_ages = [x.age for x in all_males]
    # print("bearFather: number of available males by age category: {}".format([len(list(group)) for key, group in groupby(all_ages)]))
    # print("bearFather: number of available males by age: {}".format(collections.Counter(all_ages)))
    ###

    # Mate each male `me` times.
    while True:
        for i in range(me):
            yield all_males[0]
            # print("male of age: {}".format(all_males[0].age))
        all_males = all_males[1:] # after mating "me" times, remove animal from the pool
        # after = len(all_males)
        # print("bearFather: num of males before/after removal: {}".format(before - after))

    # while True:
    #     for i in range(len(all_males)):
    #         for j in range(me):
    #             print("male of age: {}".format(all_males[0].age))
    #             yield all_males[i]
                # after = len(all_males)
                # print("bearFather: num of males before/after removal: {}".format(before - after))
                # print("bearFather: num of available males: {}".format(len(all_males)))

'''
Function picks mothers if bear is female, has no cubs and is older or equal to 3 years.

@author Roman Luštrik
'''

def bearMother(pop, subPop):
    # prepare females in estrus
    all_females = [x for x in pop.individuals(subPop) if x.sex() == sim.FEMALE and x.hc == 0 and x.age >= 3]

    # debugging block, remove
    # print("bearMother: number of suitable females: {}".format(len(all_females)))
    # print([x.age for x in all_females])

    # (#12) add variable that documents number of non-mating years
    # sort on that variable

    # print("bearMother: number of females in subpop {} is {}".format(subPop, len(all_females)))

    while True:
        pick_female = np.random.randint(0, (len(all_females)))
        female = all_females[pick_female]
        female.hc = 1
        female.ywc = 0
        # print("picked female with ID: {}".format(female.ind_id))

        yield female