# -*- coding: utf-8 -*-
'''
Simulation of two bear populations subjected to different management plans.
We are trying to mimic different management plans in Croatia and Slovenia.
All parametrization was done based on analysis of take data. File is available
in the /fit_age_at_cull folder, namely fit_age_at_cull.html (.Rmd source).

Some code follows example overlappingGeneration.py by Bo Peng (bpeng@mdanderson.org)
found in examples pages of simuPOP website.

@author: Roman Luštrik
'''

# import modules
import simuPOP as sim
import numpy as np
from simuPOP.utils import viewVars

# import functions from accessory files
from cull import cullCountry
from chooseParents import bearMother
from chooseParents import bearFather
from populationGenerators import calculatePopulationSize
from populationGenerators import familySizeGenerator
from bookkeeping import deathWithAge
from bookkeeping import updateYWC
from bookkeeping import splitCalcMerge
# from bookkeeping import printPopVSP
from constants import *



pop = sim.Population(size = [popHR, popSI], loci = 5,
                     infoFields = ["age", "ywc", "hc", "iscub", "father_idx", "mother_idx", "ind_id", "migrate_to"],
                     subPopNames = ["croatia", "slovenia"])

# initialize infoFields and genotypes
sim.initInfo(pop = pop, values = np.random.negative_binomial(n = 3, p = 0.4, size = popHR + popSI), infoFields = "age")
sim.initInfo(pop = pop, values = 0, infoFields = "iscub")
sim.initInfo(pop = pop, values = 0, infoFields = "ywc")
sim.tagID(pop = pop)
sim.initSex(pop, maleProp = 0.5)
sim.initGenotype(pop = pop, prop = [0.1]*10)

# To be able to migrate males of a certain age, we need to create
# virtual subpopulations. Cutoff of [2, 4] will produce class of
# 2 <= x < 4
pop.setVirtualSplitter(sim.ProductSplitter(splitters = [
    sim.SexSplitter(),
    sim.InfoSplitter(field = "age", cutoff = [2, 4])
    ]))
# printPopVSP(pop) # print the structure of virtual subpopulations

# sim.dump(pop, structure = False)
# print(fam_size)

#### Parametrization of take by age categories for two different countries. Weights were calculated from
# take from both countries. The analysis (and weights) are available in fit_age_at_cull(.html/.Rmd)).

## SLOVENIA ##
p_males_slovenia = [0.0360459570699769, 0.196849035865055, 0.265488199877078, 0.212887303360712,
                          0.0973078078307892, 0.0264789065651351, 0.0403937109249498, 0.0242188021055019,
                          0.0188263227289558, 0.0189198929880733, 0.0177646756414473, 0.0159038542756493,
                          0.0162830635524414, 0.0126324672142351]
p_females_slovenia = [0.0345903913117767, 0.222248514505418, 0.32277560388415, 0.158985777223974,
                          0.0430832557875325, 0.0387938045708554, 0.023081395810868, 0.0202179029357161,
                          0.0236732054260755, 0.0139386172036084, 0.0110279470558452, 0.0174937374427544,
                          0.0208841747370068, 0.0141295571653016, 0.0126014717237209, 0.0114849876423842,
                          0.0109896555730124]

## CROATIA ##
p_males_croatia = [0.0362354844659951, 0.196795753258223, 0.266002259820698, 0.215375587609955, 0.0954073529276384,
                   0.0262961301707055, 0.0401230237235152, 0.0255996512917601, 0.0179748913375592, 0.0176215805205075,
                   0.0182940276573477, 0.0157410367545321, 0.016494923642135, 0.0120382968194291]
p_females_croatia = [0.0348820694915908, 0.220262682704527, 0.321884772462511, 0.160199589683247, 0.0424739679673065,
                     0.0383743817125134, 0.0239604973813725, 0.020480003443405, 0.0232991166149018, 0.0143631109021373,
                     0.0113282987343137, 0.0169398084524606, 0.0208763160660493, 0.0146260923658584, 0.012038331851636,
                     0.011979819671673, 0.0120311404944964]



pop.evolve(
    preOps = [
        # sim.IdTagger(infoFields = "ind_id"),
        sim.InfoExec('age += 1'), # increase age for all individuals
        sim.PyOperator(func = updateYWC), # bookkeeping for mothers with cubs
        sim.PyOperator(func = deathWithAge), # longevity cap on age
        sim.Migrator(rate = [[0.02]],
                     mode = sim.BY_PROPORTION,
                     subPops = [(1, 1)], # Population 1: Male, 2 <= age < 4 (66) --> [1, 1]
                     toSubPops = [0],
                     begin = 1)
    ],
    matingScheme = sim.HeteroMating([
                                        sim.HomoMating(
                                            chooser = sim.CombinedParentsChooser(
                                                fatherChooser = sim.PyParentsChooser(generator = bearFather),
                                                motherChooser = sim.PyParentsChooser(generator = bearMother)
                                            ),
                                            generator = sim.OffspringGenerator(ops = [
                                                sim.InfoExec("age = 0"),
                                                sim.InfoExec("iscub = 1"),
                                                sim.ParentsTagger(),
                                                sim.IdTagger(),
                                                sim.MendelianGenoTransmitter()
                                            ],
                                                                               sexMode = sim.RANDOM_SEX, # remove?
                                                                               numOffspring = familySizeGenerator)),
                                        sim.CloneMating(subPops = sim.ALL_AVAIL, weight = -1)
                                    ],
                                    shuffleOffspring = False,
                                    subPopSize = calculatePopulationSize
    ),
    postOps = [
        # Perform cull.
        sim.PyOperator(func = cullCountry,
                       param = {"Nf": 40, "Nm": 60, "p.male": p_males_croatia, "p.female": p_females_croatia},
                       subPops = 0),
        sim.PyOperator(func = cullCountry,
                       param = {"Nf": 40, "Nm": 60, "p.male": p_males_slovenia, "p.female": p_females_slovenia},
                       subPops = 1),
        # Calculate Ne_LD using split/merge. The size of the split subpopulation for performing the calculation is
        # defined in `param`.
        sim.PyOperator(func = splitCalcMerge, param = {"croatia": 100, "slovenia": 100})
        # sim.PyOutput("---- end gen ----\n")
        # sim.PyOperator(func = calcStats)
    ],
    gen = 50
)

# sim.dump(pop, structure = True)
# viewVars(pop.dvars())