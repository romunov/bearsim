# -*- coding: utf-8 -*-

from constants import primiparity
from constants import max_age
import simuPOP as sim

'''
In each iteration, increase age of cubs and release them from their mothers at primiparity at which point, the mother
becomes available for breeding and cull.

@author Roman Luštrik
'''

def updateYWC(ind):
    # Mother and cubs are separated after 'primiparity' years, mother is available for breeding or cull.
    if ind.sex() == sim.FEMALE and ind.hc == 1 and ind.ywc >= primiparity:
        ind.hc = 0
        ind.ywc = 0

    # If female is rearing cubs, add 1 to 'ywc' (years with cubs).
    if ind.sex() == sim.FEMALE and ind.hc == 1:
        ind.ywc += 1

    # If individual is '>= primiparity', release it from its mother. Individual is available for mating and cull.
    if ind.iscub == 1 and ind.age >= primiparity:
        ind.iscub = 0

    return True

'''
This function will find number of subpopulations and virtual subpopulations and
print out, for each subpopulation, all combinations of virtual subpopulations.
Numbers in square brackets after --> show how to call a particular virtual subpopulation
combination.
'''
def printPopVSP(pop):
    for sp in range(pop.numSubPop()):
        for vsp in range(pop.numVirtualSubPop()):
            print("Population {subpop}: {vspname1} ({vspsize}) --> [{subpop}, {vsp}]".format(subpop = sp,
                                                                          vspname1 =  pop.subPopName([sp, vsp]),
                                                                          vspsize = pop.subPopSize([sp, vsp]),
                                                                          vsp = vsp
                                                                ))

'''
At the beginning of each iteration, find old individuals and remove them from the population. PyOperator works so that
it removed individuals for which function returns False.

@author Roman Luštrik (roman.lustrik@biolitika.si)
'''
def deathWithAge(ind):
    # alternative implementation would be using DiscardIf()
    if ind.age >= max_age:
        # print("deathWithAge: individual {} removed".format(ind.ind_id))
        return False
    else:
        return True


'''
This function samples individuals from a (sub)population, creates its own subpopulation where calculation of various
statistics happens.
@:param A dictionary with sample sizes. Accepts "croatia" and "slovenia".

Because Ne_LD needs to be calculated on a population, one needs to split the population into a subsample upon which
the calculations are performed. After all the calculations are done (outputted), the populations are merged yet again.

@author Roman Luštrik (roman.lustrik@biolitika.si)
'''

def splitCalcMerge(pop, param):
    # Calculate population sizes to help with splitting (see below).
    pop_size_croatia = pop.subPopSize(0)
    pop_size_slovenia = pop.subPopSize(1)
    pop_sizes = {"1":pop_size_croatia, "3":pop_size_slovenia}

    # Split population into two. First subpopulation is the main one minus the
    # sample size (ss).
    # If population size is less than 100, produce NAs.
    # print("splitCalcMerge: pop sizes: {}".format(pop_sizes))

    # Define levels in "x", namely mean, lower and upper confidence intervals.
    x_name = ["fit", "lci", "uci"]

    # sim.dump(pop, max = 10)

    # If the population becomes too small, param[""] is ignored and the population is split in half. Statistical
    # calculations are done on one half and later normally merged.
    try:
        sim.splitSubPops(pop = pop, subPops = 0, sizes = [pop_size_croatia - param["croatia"], param["croatia"]])
        sim.splitSubPops(pop = pop, subPops = 2, sizes = [pop_size_slovenia - param["slovenia"], param["slovenia"]])

        if pop.dvars().gen % 5 == 0:
            print("bookkeeping: try {}/{} (cro) {}/{} (slo)".format(pop_size_croatia, param["croatia"],
                                                                pop_size_slovenia, param["slovenia"]))
    except TypeError:
        cro_size = pop_size_croatia - pop_size_croatia/2
        cro_param = pop_size_croatia - cro_size
        si_size = pop_size_slovenia - pop_size_slovenia/2
        si_param = pop_size_slovenia - si_size

        if pop.dvars().gen % 5 == 0:
            print("bookkeeping: except {}/{} (popsize cro), {} (param cro)".format(pop_size_croatia, cro_size + cro_param, param["croatia"]))
            print("bookkeeping: except {}/{} (popsize slo), {} (param slo)".format(pop_size_slovenia, si_size + si_param, param["slovenia"]))

        # print("bookkeeping: num subpops{}".format(pop.numSubPop()))
        sim.splitSubPops(pop = pop, subPops = 0, sizes = [cro_size, cro_param])
        sim.splitSubPops(pop = pop, subPops = 2, sizes = [si_size, si_param])

    sim.stat(pop = pop, popSize = "subPopSize")
    sim.stat(pop = pop, effectiveSize = sim.ALL_AVAIL, vars = "Ne_LD_sp")

    # Construct a dictionary that can be read in columns:
    #               gen, pop_num, pop_name, x, 0.0, 0.01, 0.02, 0.05, actual_size
    # generation ____/      /      /       /   /     /     /     /      /
    # population num ______/      /       /   /     /     /     /      /
    # population name ___________/       /   /     /     /     /      /
    # variable designation _____________/   /     /     /     /      /
    # value without a cutoff ______________/     /     /     /      /
    # value at cutoff of 0.01 __________________/     /     /      /
    # value at cutoff of 0.02 _______________________/     /      /
    # value at cutoff of 0.05 ____________________________/      /
    # actual subpopulation size ________________________________/

# Output results into a results.txt file.
    with open("results.txt", mode = "a") as rf: #rf = result file

        # Run only through the "sample" population.
        for p in [1, 3]:
            data = []
            for x in range(3): # fit, lci, uci
                v = []
                for y in [0.0, 0.01, 0.02, 0.05]:
                    v.append(pop.dvars().subPop[p]["Ne_LD"][y][x])
                data.append(v)

                to_print = "{gen}, {pop}, {popname}, {x}, {data0}, {data01}, {data02}, {data05}, {actual_size}".format(
                    gen = pop.dvars().gen, pop = pop.subPopNames()[p], popname = p, x = x_name[x], data0 = data[x][0],
                    data01 = data[x][1], data02 = data[x][2], data05 = data[x][3], actual_size = pop_sizes[str(p)])
                print >> rf, to_print

    sim.mergeSubPops(pop = pop, subPops = [0, 1], name = "croatia")
    sim.mergeSubPops(pop = pop, subPops = [1, 2], name = "slovenia")

    # sim.stat(pop = pop, numOfMales = True, vars = "propOfMales_sp")
    # print("prop of males: {} {}".format(pop.dvars().subPop[0]["propOfMales"], pop.dvars().subPop[1]["propOfMales"]))

    return True