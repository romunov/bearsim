


######################DATA IMPORT####################
##ALL DATA
library(adegenet)
library(labeling) # za risanje

genfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Genotypes_ALL.txt"
metafile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Metadata_ALL.txt"

genotypes_raw=read.table(genfile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata=read.table(metafile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata$sex=as.factor(sample_metadata$sex)
genotypes=df2genind(genotypes_raw[2:21],sep="/",ind.names=genotypes_raw$sample)
genotypes@other=sample_metadata

genotypes_all = genotypes
rm(genotypes,genotypes_raw,sample_metadata)



###END - all data


##HR DATA

genfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Genotypes_HR.txt"
metafile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Metadata_HR.txt"

genotypes_raw=read.table(genfile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata=read.table(metafile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata$sex=as.factor(sample_metadata$sex)
genotypes=df2genind(genotypes_raw[2:21],sep="/",ind.names=genotypes_raw$sample)
genotypes@other=sample_metadata

genotypes_hr = genotypes
rm(genotypes,genotypes_raw,sample_metadata)

###END - aHr data

##SLO DATA

genfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Genotypes_SLO.txt"
metafile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearsSep2014_Metadata_SLO.txt"

genotypes_raw=read.table(genfile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata=read.table(metafile,sep="\t",header=T,stringsAsFactors=F)
sample_metadata$sex=as.factor(sample_metadata$sex)
genotypes=df2genind(genotypes_raw[2:21],sep="/",ind.names=genotypes_raw$sample)
genotypes@other=sample_metadata

genotypes_slo = genotypes
rm(genotypes,genotypes_raw,sample_metadata)

###END - SLO data
rm(genfile, metafile)

agecatsfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\AgeCats2014.3.txt"
agecats = read.table(agecatsfile,sep="\t",header=T)
rm(agecatsfile)

#######################END - DATA IMPORT ################################


str(genotypes_all@other)
hist(genotypes_all@other$age_final)#low data in older bears 


####MAKE AGESTRUCTURE FILES - ALL data
##folders 2002-2012 prepared beforehand!!!!
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers2.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_ALLDATA\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_all,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats,markers=markerpath,parentage_confidence=90,nboot=500,nloci=20,c(i,"Agecats2014_All"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - ALL DATA####



####MAKE AGESTRUCTURE FILES - SLO
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers2.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_SLO\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_slo,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats,markers=markerpath,parentage_confidence=90,nboot=500,nloci=20,c(i,"Agecats2014_SLO"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - SLO####


####MAKE AGESTRUCTURE FILES - HR
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers2.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_HR\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_hr,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats,markers=markerpath,parentage_confidence=90,nboot=500,nloci=20,c(i,"Agecats2014_HR"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - HR####


###GET RESULTS###

###ALL DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_ALLDATA\\"
results.all=AgestructDigestResults(resultspath,2001,2012,"Agecats2014_All")
results.all=cbind(study=rep("ALL",12),results.all)
results.all[results.all$type=="Estimate",]
#write.table(results.all[results.all$type=="Estimate",c(3,10)],"clipboard",sep="\t")

###SLO DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_SLO\\"
results.slo=AgestructDigestResults(resultspath,2001,2012,"Agecats2014_SLO")
results.slo=cbind(study=rep("SLO",12),results.slo)
#write.table(results.slo[results.slo$type=="Estimate",c(3,10)],"clipboard",sep="\t")

###HR DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2014_HR\\"
results.hr=AgestructDigestResults(resultspath,2001,2012,"Agecats2014_HR")
results.hr=cbind(study=rep("HR",12),results.hr)
results.hr[results.hr$type=="Estimate",]

results = rbind(results.slo, results.hr, results.all)
results.neat = cbind(results[results$type=="Estimate",c(1,3,5,6,7,8,9,10,11,12,13)],results[results$type=="CI95L",c(10:13)],results[results$type=="CI95U",c(10:13)] )
names(results.neat)[11:14] = paste(names(results.neat)[11:14],"_CID",sep="")
names(results.neat)[15:18] = paste(names(results.neat)[15:18],"_CIU",sep="")
row.names(results.neat)=paste(results.neat$study,results.neat$year,sep="_")

#pre-final results backup
#results.prefinal = results
#results.prefinal.neat = results.neat


#######################################################
####AGESTRUCTURE FILES - ROUND 2!!!
#############################################


#DIFFERENT AGECATS
agecatsfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\AgeCats2014.2.txt"
agecats2 = read.table(agecatsfile,sep="\t",header=T)
rm(agecatsfile)

####MAKE AGESTRUCTURE FILES - ALL data
##folders 2002-2012 prepared beforehand!!!!
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_ALLDATA\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_all,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats2,markers=markerpath,parentage_confidence=95,nboot=100,nloci=20,c(i,"agecats2.2014_All"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - ALL DATA####


####MAKE AGESTRUCTURE FILES - SLO
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_SLO\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_slo,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats2,markers=markerpath,parentage_confidence=95,nboot=100,nloci=20,c(i,"agecats2.2014_SLO"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - SLO####


####MAKE AGESTRUCTURE FILES - HR
markerpath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Markers.txt"
outfolder="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_HR\\"

for(i in 2001:2012) {
  outfile=paste(outfolder,i,"\\Agestructure.dat",sep="")
  make.agestruct.noexcl2(genotypes_hr,i,i,minrep.f=2,minrep.m=2,agecat.table=agecats2,markers=markerpath,parentage_confidence=95,nboot=100,nloci=20,c(i,"agecats2.2014_HR"),father.sampling.prop=0.5,mother.sampling.prop=0.5,outfile)
}
####END - MAKE AGESTRUCTURE FILES - HR####


###GET RESULTS 2###

###ALL DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_ALLDATA\\"
results2.all=AgestructDigestResults(resultspath,2001,2012,"Agecats2.2014_All")
results2.all=cbind(study=rep("ALL",12),results2.all)
results2.all[results2.all$type=="Estimate",]

###SLO DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_SLO\\"
results2.slo=AgestructDigestResults(resultspath,2001,2012,"Agecats2.2014_SLO")
results2.slo=cbind(study=rep("SLO",12),results2.slo)
results2.slo[results2.slo$type=="Estimate",]

###HR DATA
resultspath="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\Agestruct2_2014_HR\\"
results2.hr=AgestructDigestResults(resultspath,2001,2012,"Agecats2.2014_HR")
results2.hr=cbind(study=rep("HR",12),results2.hr)
results2.hr[results2.hr$type=="Estimate",]

results2=rbind(results2.slo,results2.hr,results2.all)
results2[results2$type=="Estimate",]

results2.neat = cbind(results2[results2$type=="Estimate",c(1,3,5,6,7,8,9,10,11,12,13)],results2[results2$type=="CI95L",c(10:13)],results2[results2$type=="CI95U",c(10:13)] )
names(results2.neat)[11:14] = paste(names(results2.neat)[11:14],"_CID",sep="")
names(results2.neat)[15:18] = paste(names(results2.neat)[15:18],"_CIU",sep="")
row.names(results2.neat)=paste(results2.neat$study,results2.neat$year,sep="_")

names(results2.neat)
results2.neat[,c(1,2,8,12,16)]

#LDNE (window=8)
ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_ALL8y.gen"
make.ldne2(genotypes_all,2001,2012,8,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_SLO8y.gen"
make.ldne2(genotypes_slo,2001,2012,8,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_HR8y.gen"
make.ldne2(genotypes_hr,2001,2012,8,ldnefile)


#LDNE (Window = 3)
ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_ALL3y.gen"
make.ldne2(genotypes_all,2001,2012,3,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_SLO3y.gen"
make.ldne2(genotypes_slo,2001,2012,3,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_HR3y.gen"
make.ldne2(genotypes_hr,2001,2012,3,ldnefile)


#LDNE (Window = 1)
ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_ALL1y.gen"
make.ldne2(genotypes_all,2001,2012,1,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_SLO1y.gen"
make.ldne2(genotypes_slo,2001,2012,1,ldnefile)

ldnefile = "C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\LDNE\\LDNE_HR1y.gen"
make.ldne2(genotypes_hr,2001,2012,1,ldnefile)

ldneData8y = read.table("clipboard",sep="\t",header=T)

ldneData3y = read.table("clipboard",sep="\t",header=T)

ldneData1y = read.table("clipboard",sep="\t",header=T)

####PLOTTING
library(ggplot2)

#plot2 - results w/ 90% assignment prob and higher error rate
pHighErr=ggplot(data=results.neat,aes(x=year,y=Ne,linetype=study,colour=study,group=study))
pHighErr=pHighErr+geom_errorbar(aes(ymin=results2.neat$Ne_CID,ymax=results2.neat$Ne_CIU,fill=study),alpha=1/2)+scale_color_manual(values=c("black","red","blue"))
#p=p+geom_pointrange(aes(ymin=NbCi.D,ymax=NbCi.U))
pHighErr=pHighErr+geom_line(size=1)+geom_point(aes(shape=study),size=2.5)+theme_bw()+xlim(2000.5,2012.5)+scale_x_continuous(breaks=2001:2012)#ylim(0,2000)+
pHighErr

#95% assign. prob, low error
pLowErr=ggplot(data=results.prefinal.neat,aes(x=year,y=Ne,linetype=study,colour=study,group=study))
pLowErr=pLowErr+geom_errorbar(aes(ymin=results2.neat$Ne_CID,ymax=results2.neat$Ne_CIU,fill=study),alpha=1/2)+scale_color_manual(values=c("black","red","blue"))
#p=p+geom_pointrange(aes(ymin=NbCi.D,ymax=NbCi.U))
pLowErr=pLowErr+geom_line(size=1)+geom_point(aes(shape=study),size=2.5)+theme_bw()+xlim(2000.5,2012.5)+scale_x_continuous(breaks=2001:2012)#ylim(0,2000)+
pLowErr

grid.arrange(pHighErr,pLowErr,ncol=1)

#plot LDNE ###TA V PREZENTACIJO!!!
p8=ggplot(data=ldneData8y,aes(x=year,y=Ne,linetype=study, color=study,group=study)) #-25 je brez 2001 hr - premalo vzorcev.
p8=p8+geom_errorbar(aes(ymin=ldneData8y$Ne_CID,ymax=ldneData8y$Ne_CIU,fill=study),alpha=1/2)+
  scale_color_manual(values=c("red2","black","royalblue4"))+
  scale_linetype_manual(values=c("dashed","solid","dashed"))
p8=p8+geom_line(size=1)+geom_point(aes(shape=study),size=2.5)+theme_bw()+ylim(0,420)+xlim(2002.5,2010.5)+ylab("Ne*")+xlab("Year")+theme(legend.position="top")
p8 = p8+scale_x_continuous(breaks=2001:2012)#+ theme(panel.margin = unit(-150, "lines"))

#plot LDNE ###TA V PREZENTACIJO!!!
p3=ggplot(data=ldneData3y,aes(x=year,y=Ne,linetype=study, color=study,group=study)) #-25 je brez 2001 hr - premalo vzorcev.
p3=p3+geom_errorbar(aes(ymin=ldneData3y$Ne_CID,ymax=ldneData3y$Ne_CIU,fill=study),alpha=1/2)+
  scale_color_manual(values=c("red2","black","royalblue4"))+
  scale_linetype_manual(values=c("dashed","solid","dashed"))
p3=p3+geom_line(size=1)+geom_point(aes(shape=study),size=2.5)+theme_bw()+ylim(0,420)+xlim(2002.5,2010.5)+ylab("Ne*")+xlab("Year")+theme(legend.position="top")
p3 = p3+scale_x_continuous(breaks=2001:2012)#+ theme(panel.margin = unit(-150, "lines"))
p3

#plot LDNE
p1=ggplot(data=ldneData1y,aes(x=year,y=Ne,linetype=study,group=study))
p1=p1+geom_errorbar(aes(ymin=ldneData1y$Ne_CID,ymax=ldneData1y$Ne_CIU,fill=study),alpha=1/2)#+scale_fill_grey()
#p=p+geom_pointrange(aes(ymin=NbCi.D,ymax=NbCi.U))
p1=p1+geom_line(size=1)+geom_point(aes(shape=study),size=2.5)+theme_bw()+ylim(0,400)+xlim(2000.5,2012.5)
p1

###age/sex/year
agefile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\AnalysisData\\BearAgeYear.txt"
ageData=read.table(agefile,sep="\t",header=T,stringsAsFactors=T)
ageData$year=ageData$Year
ageData$Year=as.factor(ageData$Year)
#agedata

library(reshape)
m.ageData=melt(ageData[,2:5], id.vars=c("year","country","sex"))
cast(m.ageData,country+sex+year~variable,median)

#age plot - males
pAgeM=qplot(data=ageData[ageData$Sex=="Male" & ageData$year>2000 & ageData$year<2013,],x=Year,y=Age,geom="boxplot",fill=Country,main="Age of Culled Bears - Males")
pAgeM=pAgeM+scale_fill_manual(values=c("indianred2","steelblue3"))+theme_bw()+ylim(0,15)+theme(legend.position="top")#+xlim(2000,2013)
pAgeM

#age plot - females
pAgeF=qplot(data=ageData[ageData$Sex=="Female" & ageData$year>2000 & ageData$year<2013,],x=Year,y=Age,geom="boxplot",fill=Country,main="Age of Culled Bears - Females")
pAgeF=pAgeF+scale_fill_manual(values=c("indianred2","steelblue3"))+theme_bw()+ylim(0,15)+theme(legend.position="top")#+xlim(2000,2013)
pAgeF

#age plot - both
pAge=qplot(data=ageData[ageData$year>2000 & ageData$year<2013,],x=Year,y=Age,geom="boxplot",fill=Country,main="Age of Culled Bears - Both Sexes")
pAge=pAge+scale_fill_manual(values=c("indianred2","steelblue3"))+theme_bw()+ylim(0,15)+theme(legend.position="top")#+xlim(2000,2013)
pAge

#numBears
pCounts=qplot(data=ageData[ageData$year>2000 & ageData$year<2013,],x=as.factor(Year),geom="bar",fill=Sex,color=Country,position="dodge",fill=Country,main="BearCounts")
pCounts=pCounts+theme_bw()+theme(legend.position="top")+scale_fill_manual(values=c("indianred2","steelblue3"))+scale_color_manual(values=c("black","red"))
pCounts



install.packages("gridExtra")
library(gridExtra)


grid.arrange(p8,pAge,pAgeM,pAgeF,ncol=1)

grid.arrange(p8,pAge,ncol=1)

###Cull graphs
cullfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\BearCullSloHr2.txt"
cullData=read.table(cullfile,sep="\t",header=T)
head(cullData)
m.cullData=melt(cullData,id.vars=c("Year","Country"), measure.vars=c("Male","Female","Total"))
names(m.cullData)[c(3,4)]=c("Sex","Mortality")

cullData2=m.cullData
#cullData2=m.cullData[m.cullData$Sex != "Total",]


mortSlo=ggplot(data=droplevels(cullData2[cullData2$Sex!="Total" & cullData$Country == "Slovenia",]),aes(x=as.factor(Year),y=Mortality,fill=Sex))+
  geom_bar(stat="identity",position="dodge",size=0.2)+theme_bw()+xlab("Year")+theme(legend.position="none")+scale_fill_manual(values=c("indianred2","steelblue3","black"))+
  geom_line(data=cullData2[cullData2$Sex=="Total" & cullData2$Country == "Slovenia", ], aes(x=as.factor(Year),group=1),colour="black", size = 1.2)+ylim(0,150)
mortSlo

mortHr=ggplot(data=droplevels(cullData2[cullData2$Sex!="Total" & cullData$Country == "Croatia",]),aes(x=as.factor(Year),y=Mortality,fill=Sex))+
  geom_bar(stat="identity",position="dodge",size=0.2)+theme_bw()+xlab("Year")+theme(legend.position="none")+scale_fill_manual(values=c("indianred2","steelblue3","black"))+
  geom_line(data=cullData2[cullData2$Sex=="Total" & cullData$Country == "Croatia",], aes(x=as.factor(Year),group=1),colour="black", size = 1.2)+ylim(0,150)
mortHr

grid.arrange(mortSlo,mortHr,ncol=1)

mortJoint = qplot(data=droplevels(cullData2[cullData2$Sex!="Total" & cullData$Country == "Croatia",]

cullfile="C:\\Users\\Bing\\Documents\\Raziskovanje\\SorodnostiMedvedov2014\\BearCullSloHrSummarized.txt"
cullDataSumm=read.table(cullfile,sep="\t",header=T)

qplot(data=cullDataSumm,x=Year,y=Total,color=Country)+geom_line(size=1.2)+
  theme_bw()+xlab("Year")+theme(legend.position="top")+scale_color_manual(values=c("indianred2","black","steelblue3"))+
  scale_x_continuous(breaks=1995:2012)
