# sent in by Bo Peng via emailing list

import simuPOP as sim
from random import random

pop = sim.Population(size = [10], loci = 3, infoFields = ["age", 'offspring_idx'])

sim.initInfo(pop = pop,
             values = 3,
             infoFields = "age")

sim.initSex(pop, maleProp = 0.5)
sim.initGenotype(pop = pop, prop = [0.1]*10)

def demoModel(pop):
    return 5 * pop.popSize()

def RemoveExtraOffspring(pop):
    print(pop.indInfo('offspring_idx'))
    to_be_removed = []
    for idx, ind in enumerate(pop.individuals()):
        if ind.offspring_idx >= 2 and random() > 0.4:
            to_be_removed.append(idx)
    pop.removeIndividuals(indexes=to_be_removed)
    print(pop.indInfo('offspring_idx'))
    return True

sim.dump(pop, structure = False)
pop.evolve(
    preOps = [
        sim.InfoExec("age += 1"),
        sim.Stat(popSize = True)
    ],
    matingScheme = sim.HeteroMating([
        sim.CloneMating(subPops = sim.ALL_AVAIL, weight = -1),
        sim.HomoMating(
            chooser = sim.CombinedParentsChooser(
                # random father
                sim.RandomParentChooser(sexChoice=sim.MALE_ONLY),
                # sequential mother, without replacement
                sim.SequentialParentChooser(sexChoice=sim.FEMALE_ONLY),
            ),
            generator = sim.OffspringGenerator(ops = [
                sim.InfoExec("age = 0"),
                # check the mother and father of each offspring
                sim.OffspringTagger(),
                sim.MendelianGenoTransmitter()
                ],
            numOffspring = 4)
            )],
        subPopSize=demoModel,
        shuffleOffspring=False, # to see what is going on easier
    ),
    postOps=sim.PyOperator(RemoveExtraOffspring),
    gen = 1
)
sim.dump(pop, structure = False)
