# Calculate the population size, which should be number of animals in population plus forseen number of individuals
# based on number of females. It is assumed all females mate.
import simuPOP as sim

def calculatePopSize(pop):
    pop.setVirtualSplitter(sim.SexSplitter())
    sim.stat(pop = pop, popSize = True, numOfMales =  True, vars = ["numOfFemales_sp", "popSize_sp"])
    print "number of females in croatia: %s" % pop.dvars().subPop[0]["numOfFemales"]
    print "population size in croatia: %s" % pop.dvars().subPop[0]["popSize"]

    print "number of females in slovenia: %s" % pop.dvars().subPop[1]["numOfFemales"]
    print "population size in slovenia: %s" % pop.dvars().subPop[1]["popSize"]
    print "============================="
    # print pop.dvars().subPopSize

    femHR = pop.dvars().subPop[0]["numOfFemales"]
    femSI = pop.dvars().subPop[1]["numOfFemales"]

    popHR = pop.dvars().subPopSize[0]
    popSI = pop.dvars().subPopSize[1]
    print "popHR: %s" % popHR

    pupHR = np.random.choice(a = [1, 2, 3, 4], size = femHR, p = [0.15, 0.5, 0.3, 0.05],
                     replace = True).tolist()
    print pupHR
    pupHR = sum(pupHR)
# find number of females
    return [10, 10]