import simuPOP as sim
from random import randint

pop = sim.Population(size = [10], loci = 3, infoFields = ["age", 'father_idx', 'mother_idx'])

sim.initInfo(pop = pop,
             values = 3,
             infoFields = "age")

sim.initSex(pop, maleProp = 0.5)
sim.initGenotype(pop = pop, prop = [0.1]*10)

famSize = []

    
def demoModel(pop):
    global famSize
    famSize = []
    for ind in pop.individuals():
        if ind.sex() == sim.FEMALE:
            famSize.append(randint(2, 4))
    # return cloned population size plus all
    # offspring
    print('FamSize: {}'.format(famSize))
    return pop.popSize() + sum(famSize)


def famSizeGenerator():
    global famSize
    for sz in famSize:
        print('producing {} cubs'.format(sz))
        yield sz

def endGenerationClick():
    print("=== END OF GENERATION TALLY ===")
    print("fam_size: {}". format(famSize))
    print("=== END OF GENERATION TALLY ===")
    return(True)

sim.dump(pop, structure = False)
pop.evolve(
    preOps = [
        sim.InfoExec("age += 1"),
        sim.Stat(popSize = True)
    ],
    matingScheme = sim.HeteroMating([
        sim.HomoMating(
            chooser = sim.CombinedParentsChooser(
                # random father
                sim.RandomParentChooser(sexChoice=sim.MALE_ONLY),
                # sequential mother, without replacement
                sim.SequentialParentChooser(sexChoice=sim.FEMALE_ONLY),
            ),
            generator = sim.OffspringGenerator(ops = [
                sim.InfoExec("age = 0"),
                # check the mother and father of each offspring
                sim.ParentsTagger(),
                sim.MendelianGenoTransmitter()
                ],
            numOffspring = famSizeGenerator)
            ),
        sim.CloneMating(subPops = sim.ALL_AVAIL, weight = -1)
                                    ],
        subPopSize=demoModel,
        shuffleOffspring=False, # to see what is going on easier
    ),
    postOps = sim.PyOperator(endGenerationClick),
    gen = 3
)
#sim.dump(pop, structure = False)
