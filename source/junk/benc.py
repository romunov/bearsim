'''
Testiram, ce v pythonu prealokacija tudi kupi cas.
'''

def nopre():
    a = []
    for i in range(0, 100):
        ri = random.random()
        a.append(ri)

def yespre():
    a = [0.0] * 100
    for i in range(0, 100):
        ri = random.random()
        a[i] = ri

if __name__ == "__main__":
    import timeit
    import random
    print "Starting first benchmark"
    print(timeit.timeit("nopre()", setup = "from __main__ import nopre", number = 10000))
    print "Starting second benchmark"
    print(timeit.timeit("yespre()", setup = "from __main__ import yespre", number = 10000))