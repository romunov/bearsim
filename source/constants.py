# -*- coding: utf-8 -*-

import math

'''
These are constants used in the simulation (simulation.py).
'''

popHR = 500
popSI = 500
population_size = popHR + popSI

primiparity = 3 # after how many years cubs become independent of their mother
me = 8 # number of mating events of father bears
# p_mate = 0.1 # number of females to mate, see issue #11 (https://bitbucket.org/romunov/bearsim/issue/11/female-bears-3-years-inaccessible-to-cull)
mate_rate = 0.696 # fraction, see calculatePopulationSize
max_age = 25 # animal longevity

# parametrize function that governs fitness in male bears
# see http://web.anglia.ac.uk/numbers/functions_and_models/logistic/logistic.pdf
# for details. Use the following code to draw the curve in R:
# lc <- function(x, K, P0, k) K/(P0 + (K-P0) * exp(1 - k*x))
# curve(lc(x, K = 10, P0 = 0.1, k = 0.5), from = 0, to = 30)
def logismod(x, K, P0, k):
    res = K/(P0 + (K-P0) * math.exp(1 - k*x))
    return res

P0 = 0.1
K = 10
k = 0.7

# To determine population size, we mate the females in advance
# and use the now known number of offspring (per female) to
# calculate population size. The downside is that we need to
# use a global variable, which may not be bullet proof robust.
fam_size = [[], []] #0 = hr, 1 = si, variable that doesn't get trimmed on each iteration
fam_size_it = [[], []] # this is a bookkeeping variable that gets trimmed on each iteration